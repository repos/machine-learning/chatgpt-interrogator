# ChatGPT Interrogator

Note: This is a clone of [ryuseisan/auto-chatgpt](https://github.com/ryuseisan/auto-chatgpt) with changes to it to test ChatGPT's ability to answer questions using the Wikimedia ChatGPT plugin.

## Installation

To use this project, you need to install the dependencies by following these steps:

1. Make sure you have Python 3.10+ installed on your system. If not, download it from [here](https://www.python.org/downloads/).

2. Clone this repository:

   ```sh
   git clone https://github.com/ryuseisan/auto-chatgpt.git
   cd auto-chatgpt
   ```

3. Install the required dependencies using either Poetry or Pip:

- With Pip:

  ```sh
  pip install .
  ```

- With Poetry:

  ```sh
  poetry install
  ```

## Environment Setup

To securely store your email address and password, create a `.env` file in the project's root directory by copying the `.env.example` file:

```sh
cp .env.example .env
```

Edit the .env file and replace YOUR_EMAIL_ADDRESS and YOUR_PASSWORD with your Google email address and password.

## Usage

1. Install the latest version of the Wikipedia ChatGPT plugin.

2. Copy `prompts.csv` with a CSV containing all the questions you want to ask.

3. Enter your own login/password info into `.env` file. This file is in `.gitignore` so it won't be pushed to Gitlab.

4. Run `poetry run prompter` or `poetry run prompter --prompt-file {projectRootDirectory}/autochatgpt/prompts.csv` to loop through the questions

## Contributing

Contributions are welcome! Please feel free to submit a pull request or create an issue to discuss any potential improvements or bug fixes.

## Disclaimer

This software is experimental in nature, and upon use, the user agrees to the following terms and conditions

1. This software is experimental and is provided without any warranty.
2. The user is solely responsible for the use of this software.
3. The developer is not responsible for any outcomes resulting from the use of this software.
4. Users agree to abide by the terms of use of OpenAI's ChatGPT. For more details, please visit [here](https://openai.com/policies/usage-policies).

If you do not agree to these terms, you should not use this software.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.
