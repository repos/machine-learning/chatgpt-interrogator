"""Automate prompts to the chatGPT interface."""
import time
from typing import Annotated

import pandas as pd
import typer
from tqdm import tqdm

from autochatgpt.chatgptbot import ChatGPTBot
from autochatgpt.types import AccountType

app = typer.Typer()


@app.command()
def send_prompts(
    account_type: Annotated[AccountType, typer.Option(help="The type of account to use")] = AccountType.standard,
    prompt_file: Annotated[str, typer.Option(help="The prompt file to read from")] = "prompts.csv",
):
    """Send prompt messages to the chatGPT bot."""
    prompts_df = pd.read_csv(prompt_file)
    bot = ChatGPTBot(headless=False, account_type=account_type)
    bot.login()
    #  bot.clear_chats()
    for prompt in tqdm(prompts_df["prompt"], desc="Sending prompts", unit="prompt"):
        bot.start_new_chat()
        bot.enable_plugin(plugin_name="Wikipedia")
        time.sleep(5)
        print(f"Sending prompt: {prompt}")
        bot.send_prompt(prompt=prompt)
        print("Prompt sent.")
        print("Waiting for response...")
        # bot.wait_gpt_response()
        response = bot.get_response()
        print(f"Response: {response}")
        activations = bot.count_activations()
        print(f"Activation count: {activations}")
        print("Checking for rate limit error...")
        bot.check_for_rate_limit_error()
        print("Waiting 1 minute to prevent rate limiting...")
        time.sleep(60)  # Added to minimize times I am rate limited
    bot.export_conversations()
 

def main():
    """Run the app."""
    app()


if __name__ == "__main__":
    main()
