"""Constants for the autochatgpt package."""

OPENAI_URL = "https://chat.openai.com/"
IMPLICIT_WAIT_TIME = 60
