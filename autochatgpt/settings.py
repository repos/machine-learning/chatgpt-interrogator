"""Model Settings."""
from functools import lru_cache

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """
    Settings for the application.

    Reads from .env file in the root directory, automatically
    changes settings names to uppercase.
    """

    email_address: str
    password: str
    okta_code: str | None = None
    premium: bool = False

    model_config = SettingsConfigDict(env_file=".env")


@lru_cache()
def get_settings() -> Settings:
    """Get cached settings."""
    return Settings()  # type: ignore[call-arg]
