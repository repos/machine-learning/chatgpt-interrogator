"""Types for the autochatgpt package."""

from enum import Enum, StrEnum


class AccountType(StrEnum):
    """The type of account to use."""

    okta = "okta"
    google = "google"
    standard = "standard"


class GPTModel(Enum):
    """The type of GPT model to use."""

    GPT35 = "GPT-3.5"
    GPT4 = "GPT-4"
