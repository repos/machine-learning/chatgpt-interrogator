"""Home page for the OpenAI chatbot."""
import time

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from autochatgpt import constants
from autochatgpt.pages.base import BasePage
from autochatgpt.settings import get_settings
from autochatgpt.types import GPTModel


class HomePage(BasePage):
    """Home page for the OpenAI chatbot."""

    def skip_start_message(self):
        """Skip the start message."""
        self.click(By.CSS_SELECTOR, "button.btn-neutral")
        self._click_nth(By.CSS_SELECTOR, "button.btn-neutral", n=1)
        self._click_nth(By.CSS_SELECTOR, "button.btn-primary", n=1)

    def clear_chats(self):
        """Clear past conversations."""
        # Will throw an exception if there aren't any past conversations
        try:
            self.click(By.XPATH, "//button[@type='button' and .//img[@alt='User']]")
            self.click(By.LINK_TEXT, "Clear conversations")
            self.click(By.LINK_TEXT, "Confirm clear conversations")
        except Exception:
            pass

    def check_for_rate_limit_error(self):
        """Check if a rate limit error message is present and if it is, wait for green light message to appear."""
        rate_limit_error_message = "You've reached the current usage cap for GPT-4."
        rate_limit_green_light = "you can now try sending your message again"
        if rate_limit_error_message in self.driver.page_source:
            print(f"Rate limit error message found: {rate_limit_error_message}. Waiting for usage cap message...")
            wait = WebDriverWait(self.driver, 12000)  # Wait for an hour for the green light message to appear
            wait.until(lambda driver: rate_limit_green_light in driver.page_source)
            print(f"Green light given: {rate_limit_green_light}, resuming...")
            self.click_button_named("Try again")
        else:
            print(f"Rate limit error message not found, continuing...")

    def enable_plugin(self, plugin_name, first_time=False):
        """Enable the Wikipedia plugin."""
        parent_button_name = "Plugins" if get_settings().premium else "Alpha"
        button = self._wait_for_element(By.XPATH, f"//li[contains(., '{parent_button_name}')]")
        action = ActionChains(self.driver)
        action.move_to_element(button).click().perform()  # Click the button
        action.move_by_offset(0, 200).click().perform()

        if first_time:
            self.click(By.XPATH, f"//li[contains(., '{parent_button_name}')]")
            self.click_button_named("No plugins enabled")
            self.click_button_named("Plugin store")
            self.click_button_named("OK")
            self.type(By.ID, 'search', plugin_name)
            self.click_button_named("Install")
            self.click(By.XPATH, '//button[contains(@class, "inline-block") and contains(@class, "text-gray-500") and contains(@class, "hover:text-gray-700")]') # noqa E501
        else:
            self.click(By.XPATH, "//button[starts-with(@id, 'headlessui-listbox-button-')]")
            target_li = self._wait_for_element(By.XPATH, f"//li[contains(., '{plugin_name}') and .//img[contains(@src, 'Wikipedia-logo-v2.svg')]]") # noqa E501
            if target_li and not target_li.get_attribute('aria-selected') == 'true':  # this is not a standard checkbox
                target_li.click()

    def get_user_elements(self):
        """Get the user elements."""
        user_elements = self._wait_for_elements(
            By.XPATH,
            '//div[contains(@class, "group w-full text-gray-800 dark:text-gray-100 border-b border-black/10 dark:border-gray-900/50 dark:bg-gray-800")]', # noqa E501
        )
        if user_elements:
            return [user_element.text for user_element in user_elements]
        else:
            return []

    def set_gpt_model(self, model_version: GPTModel):
        """Set the GPT model. Note: only works for premium."""
        if get_settings().premium:
            self.click_button_named(model_version.name)

    def send_prompt(self, prompt: str):
        """Send a prompt to the chatbot."""
        textarea = self._wait_for_element(By.CSS_SELECTOR, "textarea")
        if textarea:
            textarea.clear()
            self.type(By.CSS_SELECTOR, "textarea", prompt)
            textarea.send_keys(Keys.RETURN)

    def get_response(self) -> list[str]:
        """Get the GPT response."""
        self.driver.implicitly_wait(0)
        try:
            self._wait_for_element(By.XPATH, '//div[contains(@class, "result-streaming")]')
            self._wait_for_element_to_disappear(By.XPATH, '//div[contains(@class, "result-streaming")]')
        except TimeoutException:
            # If the element doesn't exist, continue to get the text
            pass
        finally:
            self.driver.implicitly_wait(constants.IMPLICIT_WAIT_TIME)

        gpt_elements = self._wait_for_elements(By.XPATH, '//div[contains(@class, "markdown")]')
        if gpt_elements:
            return [gpt_element.text for gpt_element in gpt_elements]
        else:
            return []

    def resume_conversation(self, chatid: str):
        """Resume a conversation."""
        resume_chat_page = constants.OPENAI_URL + f"/c/{chatid}"
        self._load_page(resume_chat_page)
        if self._get_current_url() != resume_chat_page:
            raise ValueError("Unable to load conversation page. Check if the chatid is correct.")

    def start_new_chat(self):
        """Start a new chat."""
        self.click(By.LINK_TEXT, "New chat")

    def count_activations(self):
        """Count the number of activations."""
        activations = self._find_elements(By.XPATH, "//div[contains(text(), 'Used ')]")
        return len(activations)
