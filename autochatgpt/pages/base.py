"""Base page for all pages."""
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    """Base page for all pages."""

    MAX_WAIT_TIME = 10

    def __init__(self, driver):
        """Initialize the base page."""
        self.driver = driver

    def _find_elements(self, by: str, value: str):
        """Return a list of matching elements without waiting, or an empty list if no elements match."""
        return self.driver.find_elements(by, value)

    def _wait_for_element(self, by: str, value: str, timeout: float = MAX_WAIT_TIME):
        """Wait for an element to be present."""
        try:
            return WebDriverWait(self.driver, timeout).until(EC.presence_of_element_located((by, value)))
        except TimeoutException:
            return None

    def _wait_for_elements(self, by: str, value: str):
        """Wait for all elements to be present."""
        try:
            return WebDriverWait(self.driver, self.MAX_WAIT_TIME).until(
                EC.presence_of_all_elements_located((by, value))
            )
        except TimeoutException:
            return None

    def _wait_for_element_to_disappear(self, by: str, value: str):
        """Wait until an element specified by a selector becomes unavailable."""
        try:
            WebDriverWait(self.driver, self.MAX_WAIT_TIME).until(EC.invisibility_of_element_located((by, value)))
        except TimeoutException:
            return None

    def type(self, by: str, value: str, keys: str):
        """Type keys into an element."""
        element = self._wait_for_element(by, value)
        if element:
            element.send_keys(keys)

    def click(self, by: str, value: str):
        """Click on an element."""
        element = self._wait_for_element(by, value)
        if element:
            element.click()

    def click_button_named(self, name: str):
        """Click on a button with the given name."""
        self.click(By.XPATH, f"//button[contains(., '{name}')]")

    def _click_nth(self, by: str, value: str, n: int):
        """Click on the nth element."""
        elements = self._wait_for_elements(by, value)
        if elements:
            elements[n].click()

    def _load_page(self, url: str):
        """Load a new web page in the current browser session."""
        self.driver.get(url)

    def _get_current_url(self):
        """Get the URL of the current page."""
        return self.driver.current_url
