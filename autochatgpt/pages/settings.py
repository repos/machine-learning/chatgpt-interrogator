"""Settings Page for the ChatGPT Bot."""
from selenium.webdriver.common.by import By

from autochatgpt.pages.base import BasePage


class SettingsPage(BasePage):
    """Settings page for the ChatGPT Bot."""

    def activate(self):
        """Activate the Settings page."""
        self.click(By.XPATH, "//button[@type='button' and .//img[@alt='User']]")
        self.click(By.LINK_TEXT, "Settings")

    def export_conversations(self):
        """Export conversations to a CSV file."""
        self.click_button_named("Data controls")
        self.click_button_named("Export")
        self.click_button_named("Confirm export")
        self.click_button_named("Confirm export")
        self.click_button_named("Confirm export")

    def set_chat_history_and_training(self, check: bool):
        """Enable the chat history and training."""
        # Open Data Controls settings window
        self.click(By.XPATH, '//div[@class="group relative" and @data-headlessui-state=""]')
        self.click(By.XPATH, '//a[contains(text(),"Settings")]')
        self.click_button_named("Data controls")

        check_button = self._wait_for_element(By.XPATH, "//button[@aria-checked]")
        checked_value = "false"
        if check_button:
            checked_value = check_button.get_attribute("aria-checked")
        checked_bool = True if checked_value == "true" else False

        if checked_bool != check:
            # click Chat History and Training Button
            self.click(By.XPATH, '//button[contains(@role, "switch")]')

    def close(self):
        """Close the settings window."""
        self.click(By.XPATH, '//button[contains(@class, "inline-block")]')
