"""Login to the OpenAI chatbot using a Google or Okta account."""
from selenium.webdriver.common.by import By

from autochatgpt.pages.base import BasePage


class LoginPage(BasePage):
    """Login page for the OpenAI chatbot."""

    def activate(self):
        """Activate the Login page."""
        self.click(By.CSS_SELECTOR, 'button[class="btn relative btn-primary"]')

    def _enter_password(self, password):
        # type password
        try:
            self.type(By.CSS_SELECTOR, "input[type='password']", password)
        except Exception:
            self.type(By.XPATH, '//input[@name="Passwd"]', password)

    def login_google_account(self, email_address, password):
        """Login to the OpenAI chatbot using a Google account."""
        self.click(By.CSS_SELECTOR, "button[data-provider='google']")
        self.type(By.CSS_SELECTOR, "input[type='email']", email_address)
        self._click_nth(By.TAG_NAME, "button", 3)
        self._enter_password(password)
        self._click_nth(By.TAG_NAME, "button", 1)

    def login_okta_account(self, email_address, password, security_code):
        """Login to the OpenAI chatbot using an Okta account."""
        self.type(By.CSS_SELECTOR, "input[type='text']", email_address.split("@")[0])
        self._enter_password(password)
        self.click(By.CSS_SELECTOR, '[value="Sign in"]')
        self.click(By.CSS_SELECTOR, '.authenticator-button .button.select-factor.link-button')
        self.type(By.CSS_SELECTOR, '[name="credentials.passcode"]', security_code)
        self.click(By.CSS_SELECTOR, '[value="Verify"]')
        self.click_button_named("Continue")

    def login_standard_account(self, email_address, password):
        """Login to the OpenAI chatbot using a standard account."""
        self.type(By.ID, 'username', email_address)
        self.click(By.CLASS_NAME, '_button-login-id')
        self.type(By.CSS_SELECTOR, "input[type='password']", password)
        self.click(By.CLASS_NAME, '_button-login-password')
