"""A bot that can be used to interact with the OpenAI chatbot."""
import undetected_chromedriver as uc

from autochatgpt import constants
from autochatgpt.pages.home import HomePage
from autochatgpt.pages.login import LoginPage
from autochatgpt.pages.settings import SettingsPage
from autochatgpt.settings import get_settings
from autochatgpt.types import AccountType, GPTModel


class ChatGPTBot:
    """A bot that can be used to interact with the OpenAI chatbot."""

    def __init__(
        self, headless: bool = True, okta_code: str | None = None, account_type: AccountType = AccountType.standard
    ):
        """Initialize the bot."""
        self.implicitly_wait_time = constants.IMPLICIT_WAIT_TIME
        self.driver = self.set_driver(headless, self.implicitly_wait_time)
        self.driver.get(constants.OPENAI_URL)
        self.okta_code = okta_code
        self.account_type = account_type
        self.settings = get_settings()
        self.home_page = HomePage(self.driver)
        self.login_page = LoginPage(self.driver)
        self.settings_page = SettingsPage(self.driver)

    def set_driver(self, headless: bool, wait_time: int):
        """Set the driver."""
        options = uc.ChromeOptions()
        if headless:
            options.add_argument("--headless")
        driver = uc.Chrome(options=options)
        # options.add_argument('--no-sandbox')

        # wait for the page to load
        driver.implicitly_wait(wait_time)
        return driver

    def export_conversations(self):
        """Export the conversations."""
        self.settings_page.activate()
        self.settings_page.export_conversations()
        self.settings_page.close()

    def login(self):
        """Login to the OpenAI chatbot."""
        login_page = LoginPage(self.driver)
        login_page.activate()
        email_address = self.settings.email_address
        password = self.settings.password

        match self.account_type:
            case AccountType.okta:
                login_page.login_okta_account(
                    email_address=email_address, password=password, security_code=self.settings.okta_code
                )
            case AccountType.google:
                login_page.login_google_account(email_address=email_address, password=password)
            case AccountType.standard:
                login_page.login_standard_account(email_address=email_address, password=password)
        self.home_page.skip_start_message()

    def clear_chats(self):
        """Clear the chats."""
        self.home_page.clear_chats()

    def check_for_rate_limit_error(self):
        """Check for wait limit error and wait if present."""
        self.home_page.check_for_rate_limit_error()

    def send_prompt(self, prompt: str):
        """Send a prompt to the chatbot."""
        self.home_page.send_prompt(prompt)

    def get_response(self) -> list[str]:
        """Get the response from the chatbot."""
        return self.home_page.get_response()

    def enable_plugin(self, plugin_name: str):
        """Enable a plugin."""
        self.home_page.enable_plugin(plugin_name)

    def set_gpt_model(self, model_version: GPTModel):
        """Set the GPT model."""
        self.home_page.set_gpt_model(model_version)

    def start_new_chat(self):
        """Start a new chat."""
        self.home_page.start_new_chat()

    def count_activations(self):
        """Count the number of activations."""
        return self.home_page.count_activations()
